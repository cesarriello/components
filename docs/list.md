# Form

We created a integrated form validation system, heavily inspired on [Element](https://element.eleme.io) validation. The validation system works on top of [Validate.js](https://validatejs.org), a free and open source validation library.

## Basic Usage
[cinwell website](https://cinwell.com ':include :type=iframe width=100% height=400px')