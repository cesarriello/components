# Cores

> Cores gerais

    <div class="color color-white">
        <span class="color-box"></span>
        <span class="color-label"></span>
    </div>


## Plain
```html
/*vue*/

<template>
 
    <div class="color color-white">
        <span class="color-box"></span>
        <span class="color-label">x</span>
    </div>
</template>

<script>
export default {}
</script>
```

    $black

    $gray-darkest
    
    $gray-darker
    
    $gray-dark
    
    $gray
    
    $gray-light
    
    $gray-lighter
    
    $gray-lightest


> Cores Itaucard

> Cores Credicard

> Cores Hipercard
